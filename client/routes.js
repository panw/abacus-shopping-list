Router.configure({
  layoutTemplate: 'layout'
});

Router.onBeforeAction(function() {
  if (!Meteor.user()) {
    this.render('signIn');
  }
  else {
    this.next();
  }
});

Router.map(function(){
  this.route("/", {
    name: "root",
    action: function() {
      this.render("listPanel");
    }
  });
});

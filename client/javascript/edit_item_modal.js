Template.editItemModal.events({
  "click #updateItem.btn": function(event, template) {
    event.preventDefault();
    var itemName = $("#edit-item.modal #name").val();
      Meteor.call("updateItem", Session.get("editItemId"), {
          name: itemName,
          unit: $("#edit-item.modal #unit").val(),
          quantity: $("#edit-item.modal #quantity").val(),
          description: $("#edit-item.modal #description").val(),
          tags: $("#edit-item.modal #tags").val().split(",")
      }, function() {
        $("#edit-item").modal('hide');
      });
  }
});

Template.editItemModal.helpers({
  item: function() {
    var editItemId = Session.get("editItemId");
    return Items.findOne({_id: editItemId});
  }
});

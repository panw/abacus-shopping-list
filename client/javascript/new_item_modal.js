Template.newItemModal.events({
  "click #addItem" : function(event, template) {
    var itemAttributes = {
        name: $("#new-item.modal #name").val(),
        unit: $("#new-item.modal #unit").val(),
        quantity: $("#new-item.modal #quantity").val(),
        description: $("#new-item.modal #description").val(),
        tags: $("#new-item.modal #tags").val().split(","),
        purchased: false
    };

    Meteor.call("addItem", itemAttributes, function() {
      // clear all form values when insertion is complete
      $("#new-item.modal #name").val("");
      $("#new-item.modal #unit").val("");
      $("#new-item.modal #quantity").val("");
      $("#new-item.modal #description").val("");
      $("#new-item.modal #tags").val("");
      $("#new-item").modal('hide');
    });
  }
});

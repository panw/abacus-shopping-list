// Helpers here could be used across all templates

Template.registerHelper("convertToDollar", function(centValue) {
  return (centValue/100.0).toFixed(2);
});

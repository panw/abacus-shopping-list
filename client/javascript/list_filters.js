Template.listFilters.onCreated(function() {
  Session.set("filterPurchased", "shopping");
});

Template.listFilters.events({
  "click .filter.btn": function(event, template) {
    $(".filter.btn").removeClass("active");
    $(event.currentTarget).addClass("active");

    // Based on the id of the button set appropriate filtering options
    // in the global Session variable so the shopping list view could
    // update itself with filtered results
    var filterOption = event.currentTarget.id;

    switch (filterOption) {
      case "shopping":
        Session.set("filterPurchased", "shopping");
        break;
      case "purchased":
        Session.set("filterPurchased", "purchased");
        break;
      default:
        Session.set("filterPurchased", null);
        break;
    }
  }
});

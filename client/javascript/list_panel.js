Template.listPanel.events({
  // Wait 500ms before retrieving tag input as a way to
  // guessestimate when the user is done inputting tags values
  "input #search":  _.debounce(function() {
    Session.set("searchTags", $("#search").val());
  }, 500)
});

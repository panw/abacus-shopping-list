Template.shoppingItem.events({
  "click .toggle-purchased": function(event, template) {
    var item = this;
    Meteor.call("updateItem", item._id, { purchased: !item.purchased });
  },

  "click .cancel-item": function(event, template) {
    var item = this;
    Meteor.call("cancelItem", item._id);
  },

  "click .edit-item": function(event, template) {
    var item = this;

    // set current item id in reactive global Session variable, so the 
    // editItemModal can retrieving the item and prepolluate the form
    Session.set("editItemId", item._id);

    $("#edit-item").modal("show");
  }
});

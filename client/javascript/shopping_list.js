Template.shoppingList.onCreated(function (){
    this.subscribe("items");
});

Template.shoppingList.helpers({
  items: function() {
    // Get the filtering criteria from the Session variable and determine
    // the appropriate query for the client side Mini-Mongo. Return the results
    // to be updated update shopping list template data

    var query;
    var searchTags = Session.get("searchTags") ? Session.get("searchTags").toLowerCase().split(" ") : [];
    var filterPurchased = Session.get("filterPurchased") == "purchased" ? true : false;

    if(Session.get("searchTags") && Session.get("filterPurchased")){
      query = {$and: [{purchased: filterPurchased}, {tags: {$in: searchTags}}]};
    }else if(Session.get("searchTags")){
      query = {tags: {$in: searchTags}};
    }else if(Session.get("filterPurchased")){
      query = {purchased: filterPurchased};
    }else if(searchTags.length){
        query = {tags: {$in: searchTags}};
    }else{
      query = {};
    }

    return Items.find(query, {sort: {addedAt: -1}});
  },

  totalCostCents: function() {
    var items = Items.find({purchased: false});
    var totalCents = 0;
    items.forEach(function(item){
      totalCents = totalCents + (item.priceCents * item.quantity);
    });

    return totalCents;
  }
});

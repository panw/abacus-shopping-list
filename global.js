global = {} || global;

global.method = {
  noSpacesLowerCase: function(string) {
    return string.trim().replace(" ", "").toLowerCase();;
  }
};

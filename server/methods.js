Meteor.methods({
  addItem: function(item) {
    if(! Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }

    HTTP.call("GET",
      "http://item-price.herokuapp.com/get_price",
      {query: "item="+item.name },
      function(error, result) {
        if(error){
          throw new Meteor.Error("Server Error from item-price.herokuapp.com/get_price");
        }
        var itemResult = JSON.parse(result.content);

        _.extend(item, {priceCents: (itemResult.price*100),
                        addedBy: Meteor.user().username,
                        addedAt: new Date()});

        Items.insert(item);
      }
    );
  },

  updateItem: function(itemId, updates) {
    // 'updates' is a object with names and values for the update

    if(! Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }
    Items.update(itemId, {$set: updates});
  },

  cancelItem: function(itemId) {
    if(! Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }

    Items.remove(itemId);
  }
});
